<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html lang="en-GB">
<head>
<%
    try{
	Boolean b=false;
	b=session.isNew();
	if(!b){
	    b=(session.getAttribute("auth_loginid")==null);
	    if(!b)
		b=!(Long.parseLong(session.getAttribute("auth_loginid").toString())>0);
	}
	if(b)
	    response.sendRedirect("../login.jsp?msg=You must be authorized to view the contents of this page.");
    }catch(Exception ex){
	response.sendRedirect("../AuthTerminate");
    }
%>
<title>mp3 : Student Event Portal :: Welcome</title>
<%@include file='../abstracts/pre.htm'%>
<style type="text/css" media="all">

</style>
</head>
<body>
<div id="wrapper_main" data-role="page">
    <div id="wrap_head" data-role="header"><%@include file='../abstracts/header.jsp'%></div>
    <div id="wrap_content" data-role="content">
	<form name="x0" action="#">
	    <table id="tblx">
		<tbody>
		    <tr><td style="border-top:1px solid #bbb;">&nbsp;</td></tr>
		    <tr><td><span class="bold">Student GUID</span><br />
			    <% out.print(session.getAttribute("auth_student_number")); %>
			</td>
		    </tr>
		    <tr><td>&nbsp;</td></tr>
		     <tr><td><span class="bold">Role > Username</span><br />
			    STUDENT > <% out.println(request.getRemoteUser()); %>
			</td>
		    </tr>
		    <tr><td>&nbsp;</td></tr>
		    <tr><td><span class="bold">Email ID</span><br />
			    <a target="_blank" href="mailto:<% out.print(session.getAttribute("auth_email_id")); %>"><% out.print(session.getAttribute("auth_email_id"));%></a>
			</td>
		    </tr>
		    <tr><td>&nbsp;</td></tr>
		    <tr><td><a href="../AuthTerminate" class="red">Logout</a></td></tr>
		    <tr><td style="border-bottom:1px solid #bbb;">&nbsp;</td></tr>
		</tbody>
	    </table>
	</form>
    </div>
</div>
<%@include file='../abstracts/post.htm'%>
</body>
</html>

