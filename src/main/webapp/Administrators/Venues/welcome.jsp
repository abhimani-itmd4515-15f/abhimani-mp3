<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html lang="en-GB">
<head>
<title>mp3 : Student Event Portal :: Venues</title>
<%@include file='../../abstracts/pre.htm'%>
<style type="text/css" media="all">

</style>
</head>
<body>
<div id="wrapper_main" data-role="page">
    <div id="wrap_head" data-role="header"><%@include file='../../abstracts/header.jsp'%></div>
    <div id="wrap_content" data-role="content">
	<form name="x0" action="#">
	    <table id="tblx">
		<tbody>
		    <tr><td style="border-top:1px solid #bbb;">&nbsp;</td></tr>
		    <tr><td><span class="bold">Role > User</span><br />
			    VENUES > <% out.println(request.getRemoteUser()); %>
			</td>
		    </tr>
		    <tr><td><span class="bold">Output for</span><br />
			    srvVenues.retrieveAll()
			</td>
		    </tr>
		    <tr>
			<td id="lstContents"></td>
		    </tr>
		    <tr><td>&nbsp;</td></tr>
		    <tr><td><a href="../Logout" class="red">Logout</a></td></tr>
		    <tr><td style="border-bottom:1px solid #bbb;">&nbsp;</td></tr>
		</tbody>
	    </table>
	</form>
    </div>
</div>
<%@include file='../../abstracts/post.htm'%>
<script type="text/javascript">//<![CDATA[  
$(document).ready(function(){
    $.get("List", function(contents){$("td#lstContents").html(contents);});
});
//]]></script>
</body>
</html>

