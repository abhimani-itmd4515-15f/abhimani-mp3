<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html lang="en-GB">
<head>
<%
    try{
	if(Long.parseLong(session.getAttribute("auth_loginid").toString())>0)
	    response.sendRedirect("AuthTerminate");
    }catch(Exception ex){;
    }
%>
<title>mp3 : Student Event Portal :: Login</title>
<%@include file='abstracts/pre.htm'%>
<style type="text/css" media="all">

</style>
</head>
<body>
<div id="wrapper_main" data-role="page">
    <div id="wrap_head" data-role="header"><%@include file='abstracts/header.jsp'%></div>
    <div id="wrap_content" data-role="content">
	<h2 class="heading">Student Login</h2>
	<form name="x0" method="post" action="doLogin">
	    <table id="tblx">
		<tbody>
		    <tr><td style="border-top:1px solid #bbb;">&nbsp;</td></tr>
		    <tr>
			<td><label><span class="bold">Username / Email ID</span> <span class="red">*</span> <br /><input type="text" name="txtUsername" id="txtUsername" title="Enter Username" placeholder="Enter Username" maxlength="155" <% if(request.getRemoteUser()!=null){ %>readonly="readonly"<% } %> /></label><% if(request.getRemoteUser()!=null){ %><span class="red">You cannot log in with multiple users.</span><% } %></td>
		    </tr>
		    <tr>
			<td><label><span class="bold">Password</span> <span class="red">*</span> <br /><input type="password" name="txtPassword" id="txtPassword" title="Enter Password" placeholder="Enter Password" maxlength="155" /></label></td>
		    </tr>
		    <tr><td style="border-bottom:1px solid #bbb;">&nbsp;</td></tr>
		    <tr>
			<td style="color:#bbb;">
			    <input type="submit" id="btnLogin" value="login" style="margin-left:0;padding-left:0;" /> | <input type="reset" id="reset" value="clear" />
			</td>
		    </tr>
		</tbody>
	    </table>
	</form>
    </div>
</div>
<%@include file='abstracts/post.htm'%>
</body>
</html>

