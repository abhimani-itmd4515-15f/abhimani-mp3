package edu.iit.sat.itmd4515.abhimani.mp3.web.administrators.venues;

import edu.iit.sat.itmd4515.abhimani.mp3.domain.entities.Venue;
import edu.iit.sat.itmd4515.abhimani.mp3.service.units.SrvVenues;
import java.io.IOException;
import java.io.PrintWriter;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Ankit Bhimani (abhimani) on edu.iit.sat.itmd4515.abhimani.mp3
 */
@WebServlet(name="ListVenues", urlPatterns={"/Administrators/Venues/List"})
public class ListVenues
	extends HttpServlet{
    @EJB
    SrvVenues srvVenues;

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     *
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException{
	response.setContentType("text/html;charset=UTF-8");
	try(PrintWriter out=response.getWriter()){
	    out.print("<ul class=\"\">");
	    for(Venue v:srvVenues.retrieveAll()){
		out.print("<li>");
		out.print(v.toString());
		out.print("</li>");
	    }
	    out.print("</ul>");
	}
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo(){
	return "Short description";
    }// </editor-fold>
}
