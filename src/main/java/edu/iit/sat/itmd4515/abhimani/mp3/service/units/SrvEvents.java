package edu.iit.sat.itmd4515.abhimani.mp3.service.units;

import edu.iit.sat.itmd4515.abhimani.mp3.domain.entities.Department;
import edu.iit.sat.itmd4515.abhimani.mp3.domain.entities.Event;
import edu.iit.sat.itmd4515.abhimani.mp3.domain.entities.Venue;
import edu.iit.sat.itmd4515.abhimani.mp3.service.AbstractService;
import java.util.List;
import javax.ejb.Stateless;

/**
 *
 * @author Ankit Bhimani (abhimani) on edu.iit.sat.itmd4515.abhimani.mp3
 */
@Stateless
public class SrvEvents
	extends AbstractService<Event>{
    public SrvEvents(){
	super(Event.class);
    }

    @Override
    public List<Event> retrieveAll(){
	return super.em.createNamedQuery("Events.retrieveAll", Event.class).getResultList();
    }

    @Override
    public Event findByID(long ID){
	return super.em.createNamedQuery("Events.findByID", Event.class).setParameter("PID", ID).getSingleResult();
    }

    public List<Event> retrieveByDepartment(Department dept){
	return super.em.createNamedQuery("Events.retrieveByDepartment", Event.class).setParameter("Dept", dept).getResultList();
    }

    public List<Event> retrieveByVenue(Venue ven){
	return super.em.createNamedQuery("Events.retrieveByVenue", Event.class).setParameter("Ven", ven).getResultList();
    }
}
