package edu.iit.sat.itmd4515.abhimani.mp3.service.units;

import edu.iit.sat.itmd4515.abhimani.mp3.domain.entities.Venue;
import edu.iit.sat.itmd4515.abhimani.mp3.service.AbstractService;
import java.util.List;
import javax.ejb.Stateless;

/**
 *
 * @author Ankit Bhimani (abhimani) on edu.iit.sat.itmd4515.abhimani.mp3
 */
@Stateless
public class SrvVenues
	extends AbstractService<Venue>{
    public SrvVenues(){
	super(Venue.class);
    }

    @Override
    public List<Venue> retrieveAll(){
	return super.em.createNamedQuery("Venues.retrieveAll", Venue.class).getResultList();
    }

    @Override
    public Venue findByID(long ID){
	return super.em.createNamedQuery("Venues.findByID", Venue.class).setParameter("PID", ID).getSingleResult();
    }

    public Venue findByName(String name){
	return super.em.createNamedQuery("Venues.findByName", Venue.class).setParameter("Name", name).getSingleResult();
    }
}
