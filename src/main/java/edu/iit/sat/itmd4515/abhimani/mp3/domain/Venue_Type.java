package edu.iit.sat.itmd4515.abhimani.mp3.domain;

/**
 *
 * @author Ankit Bhimani (abhimani) on edu.iit.sat.itmd4515.abhimani.mp3
 */
public enum Venue_Type{
    Open, Hall, Class, Room, Exhibition
}
