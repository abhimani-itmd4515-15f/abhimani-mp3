package edu.iit.sat.itmd4515.abhimani.mp3.web.administrators;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Ankit Bhimani (abhimani) on edu.iit.sat.itmd4515.abhimani.mp3
 */
@WebServlet(name="LoginServlet", urlPatterns={"/Administrators/Login"})
public class LoginServlet extends HttpServlet{
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException{
	try{
	    String username=request.getParameter("txtUsername"),
		    password=request.getParameter("txtPassword");
	    if((!(username==null)&&(username.length()>0))&&(!(password==null)&&(password.length()>0))){
		request.login(username, password);
		if(request.isUserInRole("DEPT_ADMIN"))
		    response.sendRedirect("Departments/welcome.jsp?msg=Welcome "+request.getRemoteUser()+".&success=true");
		if(request.isUserInRole("VENUE_ADMIN"))
		    response.sendRedirect("Venues/welcome.jsp?msg=Welcome "+request.getRemoteUser()+".&success=true");
		response.sendRedirect("login.jsp?msg=Invalid username/password.");
	    }
	}catch(Exception ex){
	    ;
	}finally{
	    try{
		response.sendRedirect("login.jsp?msg=Invalid username/password.");
	    }catch(Exception ex){
		;
	    }
	}
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo(){
	return "Short description";
    }// </editor-fold>
}
