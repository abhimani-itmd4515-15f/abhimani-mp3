package edu.iit.sat.itmd4515.abhimani.mp3.web.administrators.departments;

import edu.iit.sat.itmd4515.abhimani.mp3.domain.entities.Department;
import edu.iit.sat.itmd4515.abhimani.mp3.domain.relations.DepartmentOffice;
import edu.iit.sat.itmd4515.abhimani.mp3.service.units.SrvDepartmentOffices;
import edu.iit.sat.itmd4515.abhimani.mp3.service.units.SrvDepartments;
import java.io.IOException;
import java.io.PrintWriter;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Ankit Bhimani (abhimani) on edu.iit.sat.itmd4515.abhimani.mp3
 */
@WebServlet(name="ListDepartments", urlPatterns={"/Administrators/Departments/List"})
public class ListDepartments
	extends HttpServlet{
    @EJB
    SrvDepartments srvDepartments;

    @EJB
    SrvDepartmentOffices srvDepartmentOffices;

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     *
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException{
	response.setContentType("text/html;charset=UTF-8");
	try(PrintWriter out=response.getWriter()){
	    out.print("<ul class=\"\">");
	    for(Department d:srvDepartments.retrieveAll()){
		out.print("<li>");
		out.print(d.toString());
		if(d.getDepartmentOfficesList().size()>0){
		    out.print("<ul>");
		    for(DepartmentOffice sdo:d.getDepartmentOfficesList()){
			out.print("<li>");
			out.print(sdo.toString());
			out.print("</li>");
		    }
		    out.print("</ul>");
		}
		out.print("</li>");
	    }
	    out.print("</ul>");
	}
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo(){
	return "Short description";
    }// </editor-fold>
}
